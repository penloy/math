#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void add(char *ans, float x, float y)
{
    sprintf(ans, "%f",x+y);
}
void multiply(char *ans, float x, float y)
{
    sprintf(ans, "%f",x*y);
}
int main(int argc, const char * argv[]) {

    char arr[10] = {0};

    if (argc < 4)
    {
        puts("not enough args");
    }
    else if (argc > 4)
    {
        puts("too many args\nCorrect formatting:\n math.exe [(string) add/subtract/multiply/divide] [float x] [float y]");
    }
    else
    {
        if (strcmp(argv[1], "add") == 0)
        {
            add(arr, atof(argv[2]), atof(argv[3]));
        }
        else if (strcmp(argv[1], "subtract") == 0)
        {
            add(arr, atof(argv[2]), -atof(argv[3]));
        }
        else if (strcmp(argv[1], "multiply") == 0)
        {
            multiply(arr, atof(argv[2]), atof(argv[3]));
        }
        else if (strcmp(argv[1], "divide") == 0)
        {
            multiply(arr, atof(argv[2]), 1/atof(argv[3]));
        }
        else {
            printf("Wrong keyword! First argument must be add/subtract/multiply/divide");
        }
        puts(arr);
    }
    return 0;
}
